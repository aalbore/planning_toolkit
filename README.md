# README #

#ROS Package of the planning toolkit
#Alexandre Albore 2014

### Intro ###

ROS-LAPKT, planning toolikit integration within ROS platform.
Tested with fuerte, groovy and hidro.
version 1.0 used in ICAPS 2015 paper


* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

1. Compile libaptk and libff as described in the Toolkit instructions
2. Copy the resulting files in /opt/ros/groovy/lib
3. catkin_make and catkin_make install the package
4. in your CMakeList.txt, append the following lines:


```
#!bash
find_package(catkin REQUIRED COMPONENTS
  ...
  roscpp
  planning_toolkit )

catkin_package(
  INCLUDE_DIRS ...
  LIBRARIES ...
  CATKIN_DEPENDS ... planning_toolkit
  DEPENDS system_lib 
)

include_directories( ${CATKIN_GLOBAL_LIB_DESTINATION}/include/planning_toolkit/)

target_link_libraries(<your_executable> planning_toolkit)
target_link_libraries(<your_executable> ${catkin_LIBRARIES})
target_link_libraries(<your_executable> libaptk.a)
#target_link_libraries(talker libaptk-base.a)
target_link_libraries(<your_executable> libff.a) 

```

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Alexandre Albore (alexandre.albore@onera.fr)
* Other community or team contact