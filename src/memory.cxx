/*
Lightweight Automated Planning Toolkit
Copyright (C) 2012
Miquel Ramirez <miquel.ramirez@rmit.edu.au>
Nir Lipovetzky <nirlipo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <aptk/memory.hxx>
#include "ros/console.h"

namespace aptk
{

        void report_memory_usage()
        {
                struct rusage usage_report;
                getrusage( RUSAGE_SELF, &usage_report );
                ROS_INFO("---Memory usage report---");
                ROS_INFO("Max. Resident Set Size: %ld", usage_report.ru_maxrss);
                ROS_INFO("Shared Memory Size: %ld", usage_report.ru_ixrss);
                ROS_INFO("Unshared Data Size: %ld", usage_report.ru_idrss);
                ROS_INFO("Unshared Stack Size: %ld", usage_report.ru_isrss);
        }

}
