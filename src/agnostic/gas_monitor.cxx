#include <aptk/agnostic/monitor.hxx>
#include <cmath>

namespace aptk
{
class Monitor
{

        // Default constructor
        Monitor()
        {
                m_numeric_var = 100.0;
                m_limit = 0.0;
        }


        Monitor( fluent p, float l)
                : m_numeric_var( p ),
                  m_limit( l )
        {
        }

        ~Monitor()
        {
        }

public:
        float pose_x = 0.0;
        float pose_y = 0.0;

        float distanceTo( float new_x, float new_y) {

                return (abs(new_x - pose_x) + abs(new_y - pose_y));
        }

        // Updates the current position.
        void set_current_position( float x, float y)
        {
                // Multiplication parameter
                float a = 1.0;

                // Gas is used to move. 
                update( numeric_var() - a*distanceTo(x, y) );               
                pose_x = x;
                pose_y = y;
        }


        // Empty implementation
        bool satisfies( const State& s ) const
        {
                return numeric_var() < m_limit;
        }
 
        // Basic implementation 
        bool satisfies( const Fluent_Vec& fv ) const
        {
                // extracts pose from fv
                // Checks the gas

                return numeric_var() < m_limit;
        }

        bool satisfies() const
        {
                return numeric_var() < m_limit;
        }


}
}
