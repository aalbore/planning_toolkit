/*
  Lightweight Automated Planning Toolkit
  Copyright (C) 2014
  Miquel Ramirez <miquel.ramirez@rmit.edu.au>
  Nir Lipovetzky <nirlipo@gmail.com>
  Monitor class by:
  Alexandre Albore <alexandre.albore@onera.fr>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __APTK_MONITOR__
#define __APTK_MONITOR__

#include <aptk/agnostic/strips_prob.hxx>
#include <aptk/agnostic/types.hxx>
#include <aptk/agnostic/fluent.hxx>
#include <aptk/agnostic/monitor.hxx>
#include <iostream>

namespace aptk
{

        class Monitor
        {
        public:
                Monitor( );
                Monitor( float f, float l );
                virtual ~Monitor() {};

                size_t numeric_var() const;
                void   update( float f);
                virtual bool satisfies( const State& s ) const = 0;
                virtual bool satisfies( const Fluent_Vec& fv ) const = 0;
                virtual bool satisfies() const = 0;
 
        protected:
                float 	m_numeric_var;
                float   m_limit;
        };

        inline	size_t Monitor::numeric_var() const {
                return m_numeric_var;
        }

        inline void Monitor::update( float f ) 
        {
                m_numeric_var = f;
        }

}

#endif // Monitor.hxx
